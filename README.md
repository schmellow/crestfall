Crestfall is a simple command line tool for Eve-Online Alliance Tournament statistics query and replay analysis through CREST API.

Current version: 0.2

[Binary version](http://rghost.ru/8FzTcfKTn)

Application is built and tested on .NET 4.5.
In theory it should work on Mono.

Implemented features:

1. Tournament list;

2. Team list for selected tournament;

3. Team stats for selected team;

4. Match stats for selected match of a team, which includes simple active module aggregation and ship status events;

5. Search action to perform search for a team in all tournaments

6. Local cache

Planned features:

v0.3: Better db backend, transition to template engine for text output, term dictionary

v0.4: html?

Known issues:

1. Database file size grows after repeated --update option usage. Probably unfixable since NDatabase saves history and does not support proper garbage collection.