﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestfall.Output;
using Crestfall.DataTypes;

namespace Crestfall
{
    class SearchCommand
    {
        public static void Execute(string key, bool forceNet)
        {
            // get list of team lists
            TournamentCollection tc = TournamentCollection.getTournamentCollection(forceNet);
            var tournaments = new Dictionary<string, Tournament>();
            foreach(string tkey in tc.Names.Keys)
            {
                TournamentBuilder b = new TournamentBuilder(tc);
                ProgressBar bar = new ProgressBar(b);
                tournaments.Add(tkey, b.getTournament(tkey, forceNet));
            }
            var foundKeys = new List<string>();
            // perform search on team lists by key
            foreach(Tournament t in tournaments.Values)
            {
                foreach(string teamKey in t.TeamNames.Keys)
                {
                    if (t.TeamNames[teamKey].ToLower().Contains(key.ToLower())) foundKeys.Add(teamKey);
                }
            }
            // print output
            int count = foundKeys.Count;
            Console.WriteLine(string.Format("Found results: {0}", count));
            if(count > 0)
            {
                Console.WriteLine("======================");
                for(int i = 0 ; i < count; i++)
                {
                    string teamkey = foundKeys[i];
                    string tourkey = teamkey.Split(':')[0];
                    Console.WriteLine(
                        (i+1).ToString() + ") <"+teamkey + "> " + tournaments[tourkey].TeamNames[teamkey] + 
                        " in " + tournaments[tourkey].Name);
                }
            }
        }
    }
}
