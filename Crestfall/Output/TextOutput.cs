﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestfall.DataTypes;

namespace Crestfall.Output
{
    public static class TextOutput
    {
        public static string PrepareTournamentList(TournamentCollection tournaments)
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine("Available tournaments:");
            result.AppendLine("======================");
            string[] keys = tournaments.Names.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
                result.AppendLine((i + 1).ToString() + ") <" + keys[i] + "> " + tournaments.Names[keys[i]]);
            return result.ToString();
        }

        public static string PrepareTeamList(Tournament tournament)
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine(tournament.Name + " teams:");
            result.AppendLine(string.Concat(Enumerable.Repeat("=", result.Length)));
            string[] keys = tournament.TeamNames.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
                result.AppendLine((i + 1).ToString() + ") <" + keys[i] + "> " + tournament.TeamNames[keys[i]]);
            return result.ToString();
        }

        public static string PrepareTeamStats(Team team)
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine(team.Name);
            result.AppendLine("======================");
            result.AppendLine("Captain: " + team.Captain);
            result.AppendLine("Flagship: " + team.Flagship);
            result.AppendLine("======================");
            result.AppendLine("Members:");
            result.AppendLine("----------------------");
            foreach (string name in team.Members)
                result.AppendLine(name);
            result.AppendLine("======================");
            result.AppendLine("Ban frequency:");
            result.AppendLine("----------------------");
            foreach (string ship in team.Bans.Keys)
                result.AppendLine(string.Format("{0} x{1}", ship, team.Bans[ship]));
            result.AppendLine("======================");
            result.AppendLine("Enemy ban frequency:");
            result.AppendLine("----------------------");
            foreach (string ship in team.BansAgainst.Keys)
                result.AppendLine(string.Format("{0} x{1}", ship, team.BansAgainst[ship]));
            result.AppendLine("======================");
            result.AppendLine(string.Format("Matches played: {0}", team.MatchTitles.Count));
            result.AppendLine("----------------------");
            string[] keys = team.MatchTitles.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
                result.AppendLine((i + 1).ToString() + ") <" + keys[i].ToString() + "> " + team.MatchTitles[keys[i]]);
            return result.ToString();
        }

        public static string PrepareMatchStats(Match match)
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine("[B]" + match.BlueTeam + " vs [R]" + match.RedTeam);
            result.AppendLine("======================");
            result.AppendLine("Winner: " + match.Winner);
            result.AppendLine("======================");
            
            if (match.BlueBans.Length > 0)
            {
                result.AppendLine("Blue team bans:");
                result.AppendLine("----------------------");
                foreach (string ban in match.BlueBans) result.AppendLine(ban);
            }
            else result.AppendLine("Blue bans unavailable");
            result.AppendLine("======================");
            if(match.RedBans.Length > 0)
            {
                result.AppendLine("Red team bans:");
                result.AppendLine("----------------------");
                foreach (string ban in match.RedBans) result.AppendLine(ban);
            }
            else result.AppendLine("Red bans unavailable");
            result.AppendLine("======================");
            if (match.Ships != null)
            {
                List<Ship> blueShips = new List<Ship>();
                List<Ship> redShips = new List<Ship>();
                foreach (Ship s in match.Ships) { if (s.Side == ShipSide.BLUE) blueShips.Add(s); else redShips.Add(s); }
                // blue ships
                result.AppendLine("Participants from the blue side:");
                result.AppendLine("----------------------");
                foreach (Ship s in blueShips)
                {
                    result.AppendLine(string.Format("{0} in {1}", s.Pilot, s.Shipname));
                    result.AppendLine(survival(s));
                    result.AppendLine("Fitting:");
                    foreach (string module in s.Fitting.Keys)
                        result.AppendLine(string.Format("{0} x{1}", module, s.Fitting[module]));
                    if (s != match.Ships.Last()) result.AppendLine("--------------------");
                }
                // red ships
                result.AppendLine("======================");
                result.AppendLine("Participants from the red side:");
                result.AppendLine("----------------------");
                foreach (Ship s in redShips)
                {
                    result.AppendLine(string.Format("{0} in {1}", s.Pilot, s.Shipname));
                    result.AppendLine(survival(s));
                    result.AppendLine("Fitting:");
                    foreach (string module in s.Fitting.Keys)
                        result.AppendLine(string.Format("{0} x{1}", module, s.Fitting[module]));
                    if (s != match.Ships.Last()) result.AppendLine("--------------------");
                }
                result.AppendLine("======================");
                result.AppendLine("Death timeline:");
                result.AppendLine("----------------------");
                MatchEvent[] deaths = match.extractEvents(EventType.ShipDied);
                int lastTime = -1;
                foreach (MatchEvent death in deaths)
                {
                    if (death.Time > lastTime)
                    {
                        lastTime = death.Time;
                        TimeSpan t = TimeSpan.FromSeconds(lastTime);
                        result.Append(string.Format("\n{0:D2}:{1:D2}>", t.Minutes, t.Seconds));
                    }
                    result.Append(string.Format(" [{0}] {1}({2}) ", death.Side, death.Name, death.ShipName));
                }
            }
            else result.AppendLine("Replay unavailable");
            return result.ToString();
        }

        private static string survival(Ship s)
        {
            if (s.TimeShipDied == -1) return "Survived the match";
            TimeSpan full = TimeSpan.FromSeconds(s.TimeShipDied);
            TimeSpan fromShield = TimeSpan.FromSeconds(s.TimeShipDied - s.TimeShieldDamaged);
            return string.Format("Died at {0:D2}m:{1:D2}s, survived for {2:D2}m:{3:D2}s after starting receiving damage",
                full.Minutes, full.Seconds, fromShield.Minutes, fromShield.Seconds);
        }
    }
}
