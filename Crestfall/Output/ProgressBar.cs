﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestfall.DataTypes;

namespace Crestfall.Output
{
    class ProgressBar : IProgressListener
    {
        private int maxprogress;
        private int current;
        private string message;
        private ProgressWrapper worker;
        public ProgressWrapper Worker {
            get { return worker;} 
            set
            {
                worker = value;
                if(worker != null)
                    worker.handler += new ProgressBarEventHandler(Dispatch);
            }
        }

        public ProgressBar(ProgressWrapper worker = null)
        {
            this.current = 0;
            Worker = worker;
        }

        public void Dispatch(object sender, ProgressBarEvent e)
        {
            if (e.Type == ProgressEventType.UPDATE)
                updateProgress();
            else if (e.Type == ProgressEventType.SETMAX)
                maxprogress = e.Value;
            else if (e.Type == ProgressEventType.ADDMAX)
                maxprogress += e.Value;
            else if (e.Type == ProgressEventType.SETMESSAGE)
                message = e.Message + ":|";
            else if (e.Type == ProgressEventType.START)
                start();
            else if (e.Type == ProgressEventType.FINISH)
                finish();
            else if (e.Type == ProgressEventType.START)
                start();
        }

        private void updateProgress()
        {
            current++;
            DrawProgressBar();
        }

        private void start()
        {
            Console.Clear();
            DrawProgressBar();
        }

        private void finish()
        {
            worker.handler -= new ProgressBarEventHandler(Dispatch);
            Console.Clear();
        }

        // code snippet from Mark Mintoff (http://markmintoff.com/2012/09/c-console-progress-bar/)
        private void DrawProgressBar()
        {
            char progressCharacter = '=';
            int barSize = 20;
            Console.CursorVisible = false;
            int left = Console.CursorLeft;
            decimal perc = (decimal)current / (decimal)maxprogress;
            int chars = (int)Math.Floor(perc / ((decimal)1 / (decimal)barSize));
            string p1 = String.Empty, p2 = String.Empty;

            for (int i = 0; i < chars; i++) p1 += progressCharacter;
            for (int i = 0; i < barSize - chars; i++) p2 += progressCharacter;
            Console.Write(message);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(p1);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Write(p2);

            Console.ResetColor();
            Console.Write(" {0}% ({1}/{2})", (perc * 100).ToString("N2"), current, maxprogress);
            Console.CursorLeft = left;
        }
    }
}
