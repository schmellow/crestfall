﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestfall.DataTypes;

namespace Crestfall.Output
{
    interface IProgressListener
    {
        void Dispatch(object sender, ProgressBarEvent e);
    }
}
