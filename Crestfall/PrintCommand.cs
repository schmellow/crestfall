﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Crestfall.Output;
using Crestfall.DataTypes;

namespace Crestfall
{
    class PrintCommand
    {
        public static void Execute(string key, string output, bool forceNet, bool silent)
        {
            string result = null;
            if(output != null && output != "txt" && output != "term" && output != "")
            {
                Console.WriteLine("Invalid output: " + output + " is not supported");
                return;
            }
            int code = parseKey(key);
            ProgressBar bar = new ProgressBar();
            if (code == -1)
            {
                Console.WriteLine("Invalid key");
                return;
            }
            else if (code == 0)
            {
                TournamentCollection c = TournamentCollection.getTournamentCollection(forceNet);
                if (c != null) result = TextOutput.PrepareTournamentList(c);
            }
            else if (code == 1)
            {
                TournamentBuilder b = new TournamentBuilder();
                if(!silent) bar.Worker = b;
                Tournament t = b.getTournament(key, forceNet);
                if (t != null) result = TextOutput.PrepareTeamList(t);
            }
            else if (code == 2)
            {
                TeamBuilder b = new TeamBuilder();
                if (!silent) bar.Worker = b;
                Team t = b.getTeam(key, forceNet, silent);
                if (t != null) result = TextOutput.PrepareTeamStats(t);
            }
            else if (code == 3)
            {
                MatchBuilder b = new MatchBuilder();
                if (!silent) bar.Worker = b;
                Match m = b.getMatch(key, forceNet, silent);
                if (m != null) result = TextOutput.PrepareMatchStats(m);
            }
            if (result == null) return;
            if (output == "txt") // in case of html just append output to file name instead of static name?
            {
                using (var fileStream = new StreamWriter("report.txt"))
                {
                    fileStream.Write(result);
                    Console.WriteLine("Saved result to report.txt");
                }
            }
            else Console.WriteLine(result);
        }

        static int parseKey(string key)
        {
            if (key == "") return 0;
            char[] tokens = key.ToCharArray();
            // only allow those numeric literals and ':' separator
            foreach (char c in tokens)
            {
                if ((c < '0' || c > '9') && c != ':') return -1;
            }
            // check formatting to always be 'x' or 'x:y' or 'x:y:z'
            for (int i = 0; i < tokens.Length; i++)
            {
                char c = tokens[i];
                if (c == ':')
                {
                    // do not allow ':' as first and last character
                    if (i == 0 || i == tokens.Length - 1) return -1; 
                    // do not allow non number characters at ':' sides
                    if ((tokens[i - 1] < '0' || tokens[i - 1] > '9') ||
                        (tokens[i + 1] < '0' || tokens[i + 1] > '9')) return -1;
                }
            }
            int segments = key.Split(':').Length;
            if (segments > 3) return -1;
            return segments;
        }
    }
}
