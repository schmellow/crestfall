﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using clipr;

// TODO:
// v0.2 - local database
// v0.3 - term dictionary for fittings readability
// v0.4 - use template engine? html output?

namespace Crestfall
{
    class Crestfall
    {
        static void Main(string[] args)
        {
            Options opt;
            try
            {
                opt = CliParser.Parse<Options>(args);
            }
            catch(Exception e)
            {
                if(e.GetType() != typeof(clipr.Core.ParserExit))
                    Console.WriteLine(e.Message);
                return;
            }
            string key;
            if (opt.Key.Count() == 0) key = "";
            else key = opt.Key.First();
            if(opt.Search)
            {
                SearchCommand.Execute(key, opt.ForceNet);
            }
            else
            {
                PrintCommand.Execute(key, opt.Output, opt.ForceNet, opt.Silent);
            }
        }
    }
}
