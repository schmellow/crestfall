﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using clipr;

namespace Crestfall
{
    class Options
    {
        [NamedArgument('u', "update", Action = ParseAction.StoreTrue, Description = "Force network usage")]
        public bool ForceNet { get; set; }

        [NamedArgument('s', "search", Action = ParseAction.StoreTrue, Description = "Perform search action")]
        public bool Search { get; set; }

        [NamedArgument('o', "output", Action = ParseAction.Store, Description = "Select output: term (default), txt")]
        public string Output { get; set; }

        [NamedArgument("silent", Action = ParseAction.StoreTrue, Description = "Force silent mode (no progress report)")]
        public bool Silent { get; set; }
        
        [PositionalArgument(0, MetaVar = "Key", NumArgs = 1, Constraint = NumArgsConstraint.AtMost, 
            Description = "Key to query")]
        public IEnumerable<string> Key { get; set; }
    }
}
