﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Crestfall.Providers
{
    static class RestClient
    {
        private const string rootUrl = "http://public-crest.eveonline.com/tournaments/";
        private static WebClient client;
        static RestClient()
        {
            client = new WebClient();
        }

        public static JObject Root
        {
            get
            {
                return getJson(rootUrl);
            }
        }

        public static JObject getJson(string url)
        {
            string content = getRawJson(url);
            if(content == null)
            {
                Console.WriteLine("Network problems. Check your network status or try again later.");
                Environment.Exit(1);
            }
            return JsonConvert.DeserializeObject<JObject>(content);
        }

        private static string getRawJson(string url, int tries = 10)
        {
            string content;
            try
            {
                content = client.DownloadString(url);
            }
            catch (WebException e)
            {
                tries--;
                if (tries > 0)
                    content =  getRawJson(url, tries);
                else
                {
                    Console.Clear();
                    Console.WriteLine(e.Message);
                    return null;
                }
            }
            return content;
        }
    }
}
