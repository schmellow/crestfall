﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NDatabase;
using NDatabase.Api;
using Crestfall.DataTypes;

namespace Crestfall.Providers
{
    class DatabaseClient
    {
        private static IOdb odb;

        static DatabaseClient()
        {
            odb = OdbFactory.Open("crest.ndb");
        }

        public static T ExecuteQuery<T>(string key) where T : class, ICrestStorable<T>
        {
            var query = odb.Query<T>();
            int i = query.Execute<T>().Count();
            query.Descend("ID").Constrain(key).Equal();
            var resultSet = from CrestObject in odb.AsQueryable<T>()
                            where CrestObject.ID.Equals(key)
                            select CrestObject;
            if (resultSet.Count() == 0)
                return default(T);
            else return resultSet.First();
        }

        public static void Store<T>(T valueToStore) where T : class, ICrestStorable<T>
        {
            T existing = ExecuteQuery<T>(valueToStore.ID);
            if (existing != null)
            {
                existing.CopyFrom(valueToStore);
                odb.Store<T>(existing);
            }
            else odb.Store<T>(valueToStore);
            odb.Commit();
        }
    }
}
