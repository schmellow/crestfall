﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestfall.Output;

namespace Crestfall.DataTypes
{
    public enum ProgressEventType { START, FINISH, UPDATE, SETMAX, ADDMAX, SETMESSAGE }
    public delegate void ProgressBarEventHandler(object sender, ProgressBarEvent e);

    public class ProgressBarEvent : EventArgs
    {
        public ProgressEventType Type { get; set; }
        public int Value { get; set; }
        public string Message { get; set; }
    }
    
    class ProgressWrapper
    {
        public event ProgressBarEventHandler handler;

        protected ProgressWrapper()
        {
            
        }

        protected void OnEvent(ProgressBarEvent e)
        {
            if (handler != null) handler(this, e);
        }

        protected void SetMax(int max)
        {
            ProgressBarEvent e = new ProgressBarEvent();
            e.Type = ProgressEventType.SETMAX;
            e.Value = max;
            OnEvent(e);
        }

        protected void AddMax(int delta)
        {
            ProgressBarEvent e = new ProgressBarEvent();
            e.Type = ProgressEventType.ADDMAX;
            e.Value = delta;
            OnEvent(e);
        }

        protected void SetMessage(string message)
        {
            ProgressBarEvent e = new ProgressBarEvent();
            e.Type = ProgressEventType.SETMESSAGE;
            e.Message = message;
            OnEvent(e);
        }

        protected void Start()
        {
            ProgressBarEvent e = new ProgressBarEvent();
            e.Type = ProgressEventType.START;
            OnEvent(e);
        }

        protected void Finish()
        {
            ProgressBarEvent e = new ProgressBarEvent();
            e.Type = ProgressEventType.FINISH;
            OnEvent(e);
        }

        protected void UpdateProgress()
        {
            ProgressBarEvent e = new ProgressBarEvent();
            e.Type = ProgressEventType.UPDATE;
            OnEvent(e);
        }
    }
}
