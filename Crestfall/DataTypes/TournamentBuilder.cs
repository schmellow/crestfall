﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Crestfall.Providers;
using Crestfall.Output;

namespace Crestfall.DataTypes
{
    class TournamentBuilder : ProgressWrapper
    {
        private TournamentCollection tournaments;
        public TournamentBuilder(TournamentCollection tlist = null)
        {
            tournaments = tlist;
        }

        public Tournament getTournament(String key, bool forceNet)
        {
            if(!forceNet)
            {
                var dbTournament = DatabaseClient.ExecuteQuery<Tournament>(key);
                if (dbTournament != null) return dbTournament;
            }
            if(tournaments == null) tournaments = TournamentCollection.getTournamentCollection(forceNet);
            if (tournaments == null) return null;
            if (int.Parse(key) > tournaments.Links.Count)
            {
                Console.WriteLine("No such tournament with key " + key);
                return null;
            }
            // Setup own progress bar
            SetMessage("Downloading data for " + tournaments.Names[key]);
            SetMax(1);
            Start();
            //
            JToken[] entries;
            try
            {
                string url = tournaments.Links[key];
                entries = RestClient.getJson(url)["entries"].ToArray();
                UpdateProgress();
            }
            catch
            {
                Console.WriteLine("Tournament data is unreachable");
                return null;
            }
            Dictionary<string, string> names = new Dictionary<string, string>();
            Dictionary<string, string> links = new Dictionary<string, string>();
            for (int i = 0; i < entries.Length; i++)
            {
                string newkey = key + ":" + (i + 1).ToString();
                names.Add(newkey, entries[i].Value<string>("name"));
                links.Add(newkey, entries[i]["teamStats"].Value<string>("href"));
            }
            Finish();
            var result = new Tournament(key, tournaments.Names[key], names, links);
            DatabaseClient.Store<Tournament>(result);
            return result;
        }
    }
}
