﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Crestfall.DataTypes
{
    public enum ShipSide {BLUE, RED}
    public class Ship
    {
        public string Pilot { get; private set; }
        public string Shipname { get; private set; }
        public ShipSide Side { get; private set; }
        public Dictionary<string, int> Fitting { get; private set; }
        public int TimeShieldDamaged;
        public int TimeArmorDamaged;
        public int TimeStructureDamaged;
        public int TimeShipDied;

        public Ship(string Pilot, string Shipname, ShipSide Side)
        {
            this.Pilot = Pilot;
            this.Shipname = Shipname;
            this.Side = Side;
            Fitting = new Dictionary<string, int>();
            TimeShieldDamaged = -1; TimeArmorDamaged = -1;
            TimeStructureDamaged = -1; TimeShipDied = -1;
        }

        public void UpdateFitting(JToken shipFrame)
        {
            Dictionary<string, int> frameFitting = new Dictionary<string, int>(); ;
            if(shipFrame["effects"] != null)
            {
                JToken[] effects = shipFrame["effects"].ToArray();
                foreach (JToken effect in effects)
                {
                    string guid = effect.Value<string>("guid");
                    if (frameFitting.ContainsKey(guid)) frameFitting[guid]++;
                    else frameFitting.Add(guid, 1);
                }
            }
            if (shipFrame["drones"] != null)
            {
                JToken[] effects = shipFrame["drones"].ToArray();
                foreach (JToken effect in effects)
                {
                    string guid = effect["type"].Value<string>("name");
                    if (frameFitting.ContainsKey(guid)) frameFitting[guid]++;
                    else frameFitting.Add(guid, 1);
                }
            }
            if (shipFrame["missiles"] != null)
            {
                JToken[] effects = shipFrame["missiles"].ToArray();
                foreach (JToken effect in effects)
                {
                    string guid = effect["type"].Value<string>("name");
                    if (frameFitting.ContainsKey(guid)) frameFitting[guid]++;
                    else frameFitting.Add(guid, 1);
                }
            }
            // merge
            foreach(string key in frameFitting.Keys)
            {
                if (Fitting.ContainsKey(key))
                {
                    if (Fitting[key] < frameFitting[key]) Fitting[key] = frameFitting[key];
                }
                else Fitting.Add(key, frameFitting[key]);
            }
        }

        public void UpdateEvents(JToken shipFrame, int time)
        {
            if(TimeShipDied == -1)
            {
                double shield = shipFrame.Value<double>("shield");
                double armor = shipFrame.Value<double>("armor");
                double structure = shipFrame.Value<double>("structure");
                if (shield == 0 && armor == 0 && structure == 0)
                    TimeShipDied = time;
                if (TimeShieldDamaged == -1)
                    if (shield < 1) TimeShieldDamaged = time;
                if (TimeArmorDamaged == -1)
                    if (armor < 1) TimeArmorDamaged = time;
                if (TimeStructureDamaged == -1)
                    if (structure < 1) TimeStructureDamaged = time;
            }
        }
    }
}
