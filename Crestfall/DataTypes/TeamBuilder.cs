﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Crestfall.Providers;
using Crestfall.Output;

namespace Crestfall.DataTypes
{
    class TeamBuilder : ProgressWrapper
    {
        public TeamBuilder()
        {
        }

        public Team getTeam(string key, bool forceNet, bool silent)
        {
            if (!forceNet)
            {
                var dbTeam = DatabaseClient.ExecuteQuery<Team>(key);
                if (dbTeam != null) return dbTeam;
            }
            // Setup progress bar before getting tournament data
            TournamentBuilder builder = new TournamentBuilder();
            ProgressBar bar;
            if (!silent) bar = new ProgressBar(builder);
            string[] ks = key.Split(':');
            Tournament tour = builder.getTournament(ks[0], forceNet);
            if (tour == null) return null;
            if (int.Parse(ks[1]) > tour.TeamLinks.Count)
            {
                Console.WriteLine("No such team with key " + key);
                return null;
            }
            // Setup own progressbar
            SetMessage("Downloading data for team " + tour.TeamNames[key]);
            SetMax(1);
            Start();
            //
            string url = tour.TeamLinks[key];
            JObject teamFrame;
            try {
                teamFrame = RestClient.getJson(url);
                UpdateProgress();
            }
            catch { Console.WriteLine("Team data is unreachable."); return null; }
            // team name, captain name, flagship (if any) and team members
            string TeamName = tour.TeamNames[key];
            string Captain = teamFrame["captain"].Value<string>("name");
            string Flagship = "none";
            if (teamFrame["flagshipType"] != null) Flagship = teamFrame["flagshipType"].Value<string>("name");
            JToken[] memberTokens = teamFrame["pilots"].ToArray();
            string[] Members = new string[memberTokens.Length];
            for (int i = 0; i < memberTokens.Length; i++)
                Members[i] = memberTokens[i].Value<string>("name");
            Array.Sort(Members);
            // bans (if any on this tournament)
            Dictionary<string, int> Bans = getBans(teamFrame, "banFrequency");
            Dictionary<string, int> BansAgainst = getBans(teamFrame, "banFrequencyAgainst"); ;
            // and finally matches
            Dictionary<string, string> MatchTitles = new Dictionary<string, string>();
            Dictionary<string, string> MatchLinks = new Dictionary<string, string>();
            JToken[] matchTokens = teamFrame["matches"].ToArray();
            AddMax(matchTokens.Length*2);
            for (int i = 0; i < matchTokens.Length; i++)
            {
                string newkey = key + ":" + (i + 1).ToString();
                string link = matchTokens[i].Value<string>("href");
                MatchLinks.Add(newkey, link);
                MatchTitles.Add(newkey, getTitle(link, TeamName));
            }
            Finish();
            var result =  new Team(key, TeamName, Captain, Flagship, Members, Bans, BansAgainst, MatchTitles, MatchLinks);
            DatabaseClient.Store<Team>(result);
            return result;
        }

        private Dictionary<string, int> getBans(JToken frame, string banType)
        {
            Dictionary<string, int> Bans = null;
            if (frame[banType] != null)
            {
                Bans = new Dictionary<string, int>();
                foreach (JToken token in frame[banType].ToArray())
                    Bans.Add(token["shipType"].Value<string>("name"), token.Value<int>("numBans"));
            }
            return Bans;
        }

        private string getTitle(string url, string TeamName)
        {
            JObject matchFrame = RestClient.getJson(url);
            UpdateProgress();
            JObject winnerFrame = RestClient.getJson(matchFrame["winner"].Value<string>("href"));
            UpdateProgress();
            string foe = null;
            string redTeam = matchFrame["redTeam"].Value<string>("teamName");
            string blueTeam = matchFrame["blueTeam"].Value<string>("teamName");
            if (TeamName == redTeam) foe = blueTeam;
            else foe = redTeam;
            string winner = winnerFrame.Value<string>("name");
            if (TeamName == winner) return "Won against " + foe;
            else return "Lost to " + foe;
        }
    }
}
