﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Crestfall.Providers;

namespace Crestfall.DataTypes
{
    public class Tournament : ICrestStorable<Tournament>
    {
        public string ID { get; set; }
        public string Name { get; private set; }
        public Dictionary<string, string> TeamNames { get; private set; }
        public Dictionary<string, string> TeamLinks { get; private set; }

        public Tournament(string ID, string Name, 
            Dictionary<string, string> TeamNames, Dictionary<string, string> TeamLinks)
        {
            this.ID = ID;
            this.Name = Name;
            this.TeamNames = TeamNames;
            this.TeamLinks = TeamLinks;
        }

        public void CopyFrom(Tournament source)
        {
            ID = source.ID;
            Name = source.Name;
            TeamNames = source.TeamNames;
            TeamLinks = source.TeamLinks;
        }
    }
}
