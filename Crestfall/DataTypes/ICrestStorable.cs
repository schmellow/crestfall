﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crestfall.DataTypes
{
    interface ICrestStorable<T>
    {
        string ID
        {
            get;
            set;
        }

        void CopyFrom(T target);
    }
}
