﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Crestfall.Providers;

namespace Crestfall.DataTypes
{
    public class TournamentCollection : ICrestStorable<TournamentCollection>
    {
        public string ID { get; set; }
        public Dictionary<string, string> Names {get; private set;}
        public Dictionary<string, string> Links { get; private set; }

        private TournamentCollection(Dictionary<string, string> tns, Dictionary<string, string> tls)
        {
            ID = "ROOT";
            Names = tns;
            Links = tls;
        }

        public void CopyFrom(TournamentCollection source)
        {
            Names = source.Names;
            Links = source.Links;
            ID = source.ID;
        }

        public static TournamentCollection getTournamentCollection(bool forceNet)
        {
            if (!forceNet)
            {
                var dbTC = DatabaseClient.ExecuteQuery<TournamentCollection>("ROOT");
                if (dbTC != null) return dbTC;
            }
            JToken[] tokens;
            try
            {
                 tokens = RestClient.Root["items"].ToArray();
            }
            catch
            {
                Console.WriteLine("Root is unreachable");
                return null;
            }
            Dictionary<string, string> names = new Dictionary<string, string>();
            Dictionary<string, string> links = new Dictionary<string, string>();
            for (int i = 0; i < tokens.Length; i++ )
            {
                string newkey = (i + 1).ToString();
                names.Add(newkey, tokens[i]["href"].Value<string>("name"));
                links.Add(newkey, tokens[i]["href"].Value<string>("href"));
            }
            var result = new TournamentCollection(names, links);
            DatabaseClient.Store<TournamentCollection>(result);
            return result;
        }
    }
}
