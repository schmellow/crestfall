﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestfall.Providers;
using Newtonsoft.Json.Linq;

namespace Crestfall.DataTypes
{
    public class Match : ICrestStorable<Match>
    {
        public string ID { get; set; }
        public string BlueTeam { get; private set; }
        public string RedTeam { get; private set; }
        public string Winner { get; private set; }
        public string[] BlueBans { get; private set; }
        public string[] RedBans { get; private set; }
        public Ship[] Ships { get; private set; }

        public Match(
            string ID, string BlueTeam, string RedTeam, string Winner,
            string[] BlueBans, string[] RedBans, Ship[] Ships)
        {
            this.ID = ID;
            this.BlueTeam = BlueTeam;
            this.RedTeam = RedTeam;
            this.Winner = Winner;
            this.BlueBans = BlueBans;
            this.RedBans = RedBans;
            this.Ships = Ships;
        }

        public void CopyFrom(Match source)
        {
            ID = source.ID;
            BlueTeam = source.BlueTeam;
            RedTeam = source.RedTeam;
            Winner = source.Winner;
            BlueBans = source.BlueBans;
            RedBans = source.RedBans;
            Ships = source.Ships;
        }

        public MatchEvent[] extractEvents(EventType Type)
        {
            var events = new List<MatchEvent>();
            foreach(Ship ship in Ships)
            {
                if (ship.TimeShieldDamaged > -1 && (Type == EventType.ShieldDamaged || Type == EventType.All)) 
                    events.Add(
                    new MatchEvent(EventType.ShieldDamaged, ship.TimeShieldDamaged, ship.Side, ship.Pilot, ship.Shipname));
                if (ship.TimeArmorDamaged > -1 && (Type == EventType.ArmorDamaged || Type == EventType.All)) 
                    events.Add(
                    new MatchEvent(EventType.ArmorDamaged, ship.TimeArmorDamaged, ship.Side, ship.Pilot, ship.Shipname));
                if (ship.TimeStructureDamaged > -1 && (Type == EventType.StructureDamaged || Type == EventType.All))
                    events.Add(
                    new MatchEvent(EventType.StructureDamaged, ship.TimeStructureDamaged, ship.Side, ship.Pilot, ship.Shipname));
                if (ship.TimeShipDied > -1 && (Type == EventType.ShipDied || Type == EventType.All))
                    events.Add(
                    new MatchEvent(EventType.ShipDied, ship.TimeShipDied, ship.Side, ship.Pilot, ship.Shipname));
            }
            return events.OrderBy(e => e.Time).ToArray();
        }
    }
}
