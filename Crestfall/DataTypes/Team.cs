﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestfall.Providers;
using Newtonsoft.Json.Linq;

namespace Crestfall.DataTypes
{
    public class Team : ICrestStorable<Team>
    {
        public string ID { get; set; }
        public string Name { get; private set; }
        public string Captain { get; private set; }
        public string Flagship { get; private set; }
        public string[] Members { get; private set; }
        public Dictionary<string, int> Bans { get; private set; }
        public Dictionary<string, int> BansAgainst { get; private set; }
        public Dictionary<string, string> MatchTitles { get; private set; }
        public Dictionary<string, string> MatchLinks { get; private set; }

        public Team(
            string ID, string Name, string Captain, string Flagship, string[] Members, 
            Dictionary<string, int> Bans, Dictionary<string, int> BansAgainst, 
            Dictionary<string, string> MatchTitles, Dictionary<string, string> MatchLinks)
        {
            // TODO: Complete member initialization
            this.ID = ID;
            this.Name = Name;
            this.Captain = Captain;
            this.Flagship = Flagship;
            this.Members = Members;
            this.Bans = Bans;
            this.BansAgainst = BansAgainst;
            this.MatchTitles = MatchTitles;
            this.MatchLinks = MatchLinks;
        }

        public void CopyFrom(Team source)
        {
            ID = source.ID;
            Name = source.Name;
            Captain = source.Captain;
            Flagship = source.Flagship;
            Members = source.Members;
            Bans = source.Bans;
            BansAgainst = source.BansAgainst;
            MatchTitles = source.MatchTitles;
            MatchLinks = source.MatchLinks;
        }
    }
}
