﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Crestfall.Providers;
using Crestfall.Output;

namespace Crestfall.DataTypes
{
    class MatchBuilder : ProgressWrapper
    {

        public MatchBuilder()
        {
            
        }

        public Match getMatch(string key, bool forceNet, bool silent)
        {
            if (!forceNet)
            {
                var dbMatch = DatabaseClient.ExecuteQuery<Match>(key);
                if (dbMatch != null) return dbMatch;
            }
            // fallback
            string[] ks = key.Split(':');
            string teamKey = ks[0] + ":" + ks[1];
            // Setup progressbar for team data builder
            TeamBuilder builder = new TeamBuilder();
            ProgressBar bar;
            if (!silent) bar = new ProgressBar(builder);
            Team team = builder.getTeam(teamKey, forceNet, silent);
            if (team == null) return null;
            if (int.Parse(ks[2]) > team.MatchLinks.Count)
            {
                Console.WriteLine("No such match with key " + key);
                return null;
            }
            // 
            JObject matchFrame = RestClient.getJson(team.MatchLinks[key]);
            JObject winnerFrame = RestClient.getJson(matchFrame["winner"].Value<string>("href"));
            string Winner = winnerFrame.Value<string>("name");
            string BlueTeam = matchFrame["blueTeam"].Value<string>("teamName");
            string RedTeam = matchFrame["redTeam"].Value<string>("teamName");
            string[] BlueBans = null;
            string[] RedBans = null;
            if (matchFrame["bans"] != null)
            {
                JToken[] BlueBanTokens = matchFrame["bans"]["blueTeam"][0]["typeBans"].ToArray();
                JToken[] RedBanTokens = matchFrame["bans"]["redTeam"][0]["typeBans"].ToArray();
                BlueBans = new string[BlueBanTokens.Length];
                RedBans = new string[RedBanTokens.Length];
                for (int i = 0; i < BlueBanTokens.Length; i++) BlueBans[i] = BlueBanTokens[i].Value<string>("name");
                for (int i = 0; i < RedBanTokens.Length; i++) RedBans[i] = RedBanTokens[i].Value<string>("name");
            }
            Ship[] Ships = null;
            if (matchFrame["firstReplayFrame"] != null)
            {
                Uri lastUri = new Uri(matchFrame["lastReplayFrame"].Value<string>("href"));
                int nOfFrames = int.Parse(lastUri.Segments.Last().Trim('/')) + 2;
                // Set up progress bar
                SetMessage("Downloading match data");
                SetMax(nOfFrames);
                Start();
                //
                List<JObject> ReplayFrames = new List<JObject>();
                // static frame
                ReplayFrames.Add(RestClient.getJson(matchFrame["staticSceneData"].Value<string>("href")));
                UpdateProgress();
                // get first frame and walk through
                ReplayFrames.Add(RestClient.getJson(matchFrame["firstReplayFrame"].Value<string>("href")));
                UpdateProgress();
                string nextUrl;
                while (ReplayFrames.Last()["nextFrame"] != null)
                {
                    nextUrl = ReplayFrames.Last()["nextFrame"].Value<string>("href");
                    ReplayFrames.Add(RestClient.getJson(nextUrl));
                    UpdateProgress();
                }
                Finish();
                processFrames(ReplayFrames, ref Ships);
            }
            var result = new Match(key, BlueTeam, RedTeam, Winner, BlueBans, RedBans, Ships);
            DatabaseClient.Store<Match>(result);
            return result;
        }

        private void processFrames(List<JObject> replay, ref Ship[] Ships)
        {
            var shipsDict = getShipsMatched(replay);
            for (int i = 1; i < replay.Count; i++)
            {
                int time = i - 1;
                JObject frame = replay[i];
                foreach (Ship ship in shipsDict.Keys)
                {
                    string shipSide;
                    if (ship.Side == ShipSide.BLUE) shipSide = "blueTeamShipData";
                    else shipSide = "redTeamShipData";
                    int index = shipsDict[ship];
                    JToken shipFrame = frame[shipSide].ToArray()[index];
                    ship.UpdateFitting(shipFrame);
                    ship.UpdateEvents(shipFrame, time);
                }
            }
            Ships = shipsDict.Keys.ToArray();
        }

        private Dictionary<Ship, int> getShipsMatched(List<JObject> replay)
        {
            JToken[] staticShips = replay.First()["ships"].ToArray();
            JToken[] redShips = replay[1]["redTeamShipData"].ToArray();
            JToken[] blueShips = replay[1]["blueTeamShipData"].ToArray();
            Dictionary<Ship, int> ships = new Dictionary<Ship, int>();
            bool found = false;
            foreach (JToken staticShip in staticShips)
            {
                string static_id = staticShip["item"].Value<string>("href");
                for (int i = 0; i < redShips.Length; i++)
                {
                    string realtime_id = redShips[i]["itemRef"].Value<string>("href");
                    if (realtime_id == static_id)
                    {
                        Ship newship = new Ship(
                            staticShip["character"].Value<string>("name"),
                            staticShip["type"].Value<string>("name"),
                            ShipSide.RED);
                        ships.Add(newship, i);
                        found = true;
                        break;
                    }
                }
                if (found) { found = false; continue; } // skip blue ships
                for (int i = 0; i < blueShips.Length; i++)
                {
                    string realtime_id = blueShips[i]["itemRef"].Value<string>("href");
                    if (realtime_id == static_id)
                    {
                        Ship newship = new Ship(
                            staticShip["character"].Value<string>("name"),
                            staticShip["type"].Value<string>("name"),
                            ShipSide.BLUE);
                        ships.Add(newship, i);
                        break;
                    }
                }
            }
            return ships;
        }
    }
}
