﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crestfall.DataTypes
{
    public enum EventType { ShieldDamaged, ArmorDamaged, StructureDamaged, ShipDied, All}
    public class MatchEvent
    {
        public EventType Type { get; private set; }
        public int Time { get; private set; }
        public ShipSide Side { get; private set; }
        public string Name { get; private set; }
        public string ShipName { get; private set; }

        public MatchEvent(EventType Type, int Time, ShipSide Side, string Name, string ShipName)
        {
            this.Type = Type;
            this.Time = Time;
            this.Side = Side;
            this.Name = Name;
            this.ShipName = ShipName;
        }
    }
}
